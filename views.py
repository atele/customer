from datetime import datetime
import urllib
import json
from pprint import pprint
from flask import Blueprint, current_app as app, render_template, redirect, url_for, request, make_response, session, \
    g, abort, flash, send_from_directory, jsonify, send_file

from sqlalchemy import desc, or_, and_
from model_package import models, forms
from services_package import authentication, payment, basic, account
from flask_login import login_required, login_user, logout_user, current_user
from flask_principal import Identity, AnonymousIdentity, identity_changed, PermissionDenied, identity_loaded, UserNeed
from utilities.utils import encrypt_dict_to_string, decrypt_string_to_dict, copy_dict
from plant import render_domain_template
from services_package.printing import return_pdf
from services_package.permissions import *
from services_package.reporting.core import customer_custom_report
from model_package.signals import *
from integrations_package.storage import mongo

c_blueprint = Blueprint('c_blueprint', __name__)
logger = app.logger

report_handles = ["devices"]

with app.app_context():
    @app.login_manager.user_loader
    def load_user(customer_id):
        print("Customer - {}".format(customer_id))
        return models.Customer.query.get(customer_id)


    def get_current_client():
        client = models.Client.query.get(1)
        if client:
            return client
        else:
            return current_user.clients.first()


    @c_blueprint.context_processor
    def main_context():
        """ Include some basic assets in the startup page """
        today = datetime.today()
        services = models.Service.query.all()
        default_resolved = True
        length = len
        if current_user.is_authenticated:
            customer_nav_clients = current_user.clients
            current_client = get_current_client
        # else:
        #     print "----------no currnet user"
        #     customer_nav_clients = models.Client.query

        # session["current_client_id"] = customer_nav_clients.first().id
        no_angular = True
        return locals()


    @c_blueprint.route('/login/', methods=["GET", "POST"])
    def login():
        form = forms.LoginForm()

        if request.method == 'POST' and form.validate_on_submit():
            username = form.data["username"]
            password = form.data["password"]
            customer = authentication.authenticate_customer(username, password)

            if customer:
                if customer.is_blocked:
                    login_error = "User has been deactivated. Please contact support team."
                    return render_domain_template("customer/auth/login.html", **locals())

                login_user(customer, remember=True, force=True)  # This is necessary to remember the user

                # clear flash messages
                session.pop('_flashes', None)

                rs = identity_changed.send(app._get_current_object(), identity=Identity(customer.id))
                # include the username and api_token in the session
                resp = redirect(url_for('.index'))

                # load default client
                client = customer.clients.first()

                if client:
                    session["current_client_id"] = client.id

                session["customer_id"] = customer.id
                session['customer_clients_id'] = [i.id for i in customer.clients]

                # Transfer auth token to the frontend for use with api requests
                resp.set_cookie("__cred__", authentication.encode_auth_token(customer.id))

                resp.set_cookie('__publicKey__', app.config['PUSH_PUBLIC_KEY'])
                g.customer = customer
                logger.info(current_user)
                return resp
            else:
                login_error = "The username or password is invalid"

        return render_domain_template('customer/auth/login.html', **locals())


    @c_blueprint.route('/forgot_password/', methods=["GET", "POST"])
    def forgot_password():
        next_url_ = request.args.get("next_url") or url_for(".index")
        form = forms.ForgotPasswordForm()
        if form.validate_on_submit():
            data = form.data
            username = data["username"]
            user = authentication.authenticate_forgot_password(username)

            if user is not None:
                token = authentication.request_user_password(user.id)
                return redirect(url_for(".reset_request_successful"))
            else:
                login_error = "This email address or username does not exist"

        return render_domain_template("customer/auth/forgot_password.html", **locals())


    @c_blueprint.route('/reset_request_successful/', methods=["GET", "POST"])
    def reset_request_successful():
        return render_domain_template("customer/auth/reset_request_successful.html", **locals())


    @c_blueprint.route('/logout/', methods=['GET'])
    @login_required
    def logout():
        """
        logout user from application
        :return:
        """
        logout_user()

        # Remove session keys set by Flask-Principal
        for key in ('identity.name', 'identity.auth_type', 'encryted_card_data', '_flashes', 'current_client_id',
                    'current_client_id', 'customer_id', 'customer_clients_id'):
            session.pop(key, None)

        # Tell Flask-Principal the user is anonymous
        identity_changed.send(app, identity=AnonymousIdentity())

        resp = redirect(url_for('.index'))
        resp.set_cookie('__cred__', '', expires=0)
        return resp


    @c_blueprint.route('/', methods=["GET"])
    @c_blueprint.route('/<path:url>/')
    @login_required
    def index(url=None):
        logger.info("in index")
        page_title = "Home"
        no_angular = True
        return render_domain_template('customer/index.html', **locals())


    @c_blueprint.route('/<slug>/overview/', methods=["GET"])
    @login_required
    def dashboard(slug):
        """
        Services dashboard
        :param slug:
        :return:
        """
        client = get_current_client()
        if slug == "devices":
            return redirect(url_for('.devices_list'))
        if slug == "billings":
            debt_threshold_form = forms.BillingThresholdForm()
            delete_account_form = forms.DeleteObjectForm()
            delete_card_form = forms.DeleteObjectForm()

            country = models.Country.query.filter(models.Country.code == "NG").first()

            bank_account_form = forms.BankAccountForm(client_id=client.id)
            bank_account_form.bank_id.choices = [(0, "--- Select Bank ---")] + [(b.id, b.name) for b in
                                                                                models.Bank.query.order_by(
                                                                                    models.Bank.name).all()]

            card_verf_form = forms.AddCardVerfForm()

            form = forms.CardForm(country_id=country.id, client_id=client.id)
            form.city_id.choices = [(0, "--- Select City ---")] + [(c.id, c.name) for c in
                                                                   models.City.query.order_by(models.City.name).all()]
            form.state_id.choices = [(0, "--- Select State ---")] + [(s.id, s.name) for s in
                                                                     models.State.query.order_by(
                                                                         models.State.name).all()]
            form.country_id.choices = [(0, "--- Select Country ---")] + [(c.id, c.name) for c in
                                                                         models.Country.query.order_by(
                                                                             models.Country.name).all()]

        return render_domain_template('customer/%s/index.html' % slug, **locals())


    @c_blueprint.route('/fetch/state_cities/', methods=["GET", "POST"])
    def fetch_state_cities():
        if request.method == "POST":
            _data = request.data
            if not _data:
                response = make_response("Request Method not Allowed")
                return response

            try:
                state = basic.StateService.get(int(_data))
                data = [{"id": i.id, "name": i.name} for i in state.cities.order_by(models.City.name)]
                refine = json.dumps(data)
                response = make_response(refine)
                return response

            except Exception as e:
                logger.info("---------Error: %s-----------" % str(e))
                msg = "Failed with Error " + str(e)
                response = make_response(msg)
                return response
        else:
            response = make_response("Request Method not Allowed")
            return response


    @c_blueprint.route('/fetch/bank_account_info/', methods=["GET", "POST"])
    def fetch_bank_account_info():
        if request.method == "POST":
            _data = request.data

            if not _data:
                response = make_response("Request Method not Allowed")
                return response
            _data = json.loads(_data)
            try:
                res = payment.FlutterBankAccountService.verify_account(**_data)
                return res

            except Exception as e:
                logger.info("---------Error: %s-----------" % str(e))
                msg = "Failed with Error " + str(e)
                response = make_response(msg)
                return response
        else:
            response = make_response("Request Method not Allowed")
            return response


    @c_blueprint.route('/clients/', methods=["GET"])
    @login_required
    def clients(slug):
        g.user = current_user

        return render_domain_template('customer/pages/index.html', **locals())


    @c_blueprint.route('/stations/list/', methods=["GET"])
    @login_required
    def stations_list():
        client = get_current_client()
        logger.info(client)

        return render_domain_template('customer/stations/list.html', **locals())


    @c_blueprint.route('/devices/list/', methods=["GET"])
    @login_required
    def devices_list():
        client = get_current_client()
        handle = "list"

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
        except:
            abort(404)

        page_title = "Devices"
        no_angular = True
        has_sub_lnk = True

        query = models.Device.query.filter(models.Device.customer_id == current_user.id,
                                           models.Device.client_id == client.id).order_by(desc(models.Device.id))

        request_args = copy_dict(request.args, {})

        results = query.paginate(page, size, False)
        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template('customer/devices/list.html', **locals())


    @c_blueprint.route('/billings/invoices/', methods=["GET"])
    @login_required
    def invoices():
        g.user = current_user
        page_title = "Invoices"
        no_angular = True
        has_sub_lnk = True
        search_q = None
        size = 20
        page = 1

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
        except Exception, e:
            print('=========error======')
            print(e)
            print('=========error======')
            abort(404)

        request_args = request.args.copy()

        query = models.CustomerInvoice.query

        query = query.order_by(desc(models.CustomerInvoice.date_created))

        results = query.paginate(page, size, False)

        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template('customer/billings/invoices.html', **locals())


    @c_blueprint.route('/billings/receipts/', methods=["GET"])
    @login_required
    def receipts():
        g.user = current_user
        page_title = "Receipts"
        no_angular = True
        has_sub_lnk = True
        search_q = None
        size = 20
        page = 1

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
        except Exception, e:
            print('=========error======')
            print(e)
            print('=========error======')
            abort(404)

        request_args = request.args.copy()

        query = models.CustomerReceipt.query

        query = query.order_by(desc(models.CustomerReceipt.date_created))

        results = query.paginate(page, size, False)

        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))
        return render_domain_template('customer/billings/receipts.html', **locals())


    @c_blueprint.route('/billings/update_debt_threshold/', methods=["GET"])
    @login_required
    def update_debt_threshold():
        g.user = current_user
        debt_threshold_form = forms.BillingThresholdForm()
        return True


    @c_blueprint.route('/billings/add_card/', methods=["GET"])
    @login_required
    def add_card():
        """
        Billing Information
        :return:
        """
        page_title = "Add Card"

        if request.method != "POST":
            flash("Request Method Not Allowed")
            return redirect(url_for('.billing_info'))

        user_id = current_user.id

        client = get_current_client()
        country = basic.CountryService.filter_by(code='NG')

        card_verf_form = forms.AddCardVerfForm()
        form = forms.CardForm(country_id=country.id, customer_id=user_id)
        form.city_id.choices = [(0, "--- Select City ---")] + [(c.id, c.name) for c in basic.CityService.all()]
        form.state_id.choices = [(0, "--- Select State ---")] + [(s.id, s.name) for s in basic.StateService.all()]
        form.country_id.choices = [(0, "--- Select Country ---")] + [(c.id, c.name) for c in
                                                                     models.Country.query.order_by(
                                                                         models.Country.name).all()]

        if form.validate_on_submit():
            data = form.data
            card_number = "XXXX-XXXX-XXXX-%s" % form.card_number.data[-4:]
            card_expiry = data.get("card_expiry", "").replace(" ", "").split("/")
            data["expiry_month"] = card_expiry[0] or ""
            data["expiry_year"] = card_expiry[1] or ""
            card = payment.CardService.filter_by(mask=card_number, brand=data.get("brand", ""),
                                                 exp_month=data.get("expiry_month", ""),
                                                 exp_year=data.get("expiry_year", ""))
            if card:
                flash("Card Record Already exist")
                return redirect(url_for('.billing_info'))

            # try:
            encryted_card_data = encrypt_dict_to_string(**data)
            session["encryted_card_data"] = str(encryted_card_data)
            status, resp = payment.FlutterCardService.register(**data)
            if resp and status == "success" and resp.get("responsemessage",
                                                         "") == "BVN must be supplied for this option":
                setting = basic.CustomerSettingsService.filter_by(customer_id=user_id)
                _d = {"billing_dep_resolved": True}
                basic.CustomerSettingsService.update(setting.id, **_d)
                flash("Card Record Added successfully")

                if not setting.stations_dep_resolved or not setting.accounts_dep_resolved or not setting.billing_dep_resolved:
                    return redirect(url_for('.onboarding'))

                return redirect(url_for('.billing_info', card=True))

                # except Exception, e:
                #     print(e)
                #     flash('Please check your internet connection, then try again!')

        return redirect(url_for('.dashboard', slug="billings"))


    @c_blueprint.route('/billings/remove_card/', methods=["GET"])
    @login_required
    def remove_card():
        g.user = current_user

        return True


    @c_blueprint.route('/billings/add_bank_account/', methods=["GET"])
    @login_required
    def add_bank_account():
        g.user = current_user

        return True


    @c_blueprint.route('/settings/', methods=["GET"])
    @login_required
    def settings():
        g.user = current_user
        page_title = "Basic Information"
        no_angular = True
        has_sub_lnk = True

        cities = basic.CityService.query.order_by(models.City.name).all()
        countries = [basic.CountryService.filter_by(slug='nigeria')]
        states = basic.StateService.query.order_by(models.State.name).all()

        form = forms.UpdateCustomerForm()
        form.city_id.choices = [(c.id, c.name) for c in cities]
        form.state_id.choices = [(c.id, c.name) for c in states]
        form.country_id.choices = [(c.id, c.name) for c in countries]

        if request.method == 'POST' and form.validate_on_submit():
            customer = account.CustomerService.update(obj_id=current_user.id, **form.data)
            return redirect(request.path)

        return render_domain_template('customer/settings/basic_info.html', **locals())


    @c_blueprint.route('/messages/', methods=["GET"])
    @c_blueprint.route('/messages/inbox/', methods=["GET"])
    def messages():
        page_title = "Messages - Inbox"
        no_angular = True
        has_sub_lnk = True
        is_inbox = True

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
            view_q = request.args.get("view", "all")
        except Exception, e:
            abort(404)

        client = get_current_client()
        search_place_holder = "Search"

        form = forms.ComposeMessageForm(customer_id=current_user.id)
        form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                             ("client", "Client")]
        form.customer_id.choices = [(current_user.id, current_user.name)]
        form.client_id.choices = [(0, "--- Choose a Client ---")] + [(i.id, i.name) for i in current_user.clients.all()]

        request_args = copy_dict(request.args, {})

        view_filters = [{"name": "all", "value": "all"}, {"name": "admin", "value": "admin"},
                        {"name": "client", "value": "client"}]

        results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                      **{'customer_id': current_user.id,
                                                         'customer_read': False})

        if view_q and view_q != "all":
            if view_q == "admin":
                is_admin_msg = True
            else:
                is_admin_msg = False
            results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                          **{'customer_id': current_user.id,
                                                             'customer_read': False, "is_admin_message": is_admin_msg})

        if results.get("next", None):
            # build next page query parameters
            request_args["page"] = results.get("next")
            results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.get("prev", None):
            # build previous page query parameters
            request_args["page"] = results.get("prev")
            results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template('customer/messages/inbox.html', **locals())


    @c_blueprint.route('/messages/outbox/', methods=["GET"])
    def outbox_messages():
        page_title = "Messages - Outbox"
        no_angular = True
        has_sub_lnk = True
        is_outbox = True

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
            view_q = request.args.get("view", "all")
        except Exception, e:
            abort(404)

        client = get_current_client()
        search_place_holder = "Search"

        form = forms.ComposeMessageForm(customer_id=current_user.id)
        form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                             ("client", "Client")]
        form.customer_id.choices = [(current_user.id, current_user.name)]
        form.client_id.choices = [(0, "--- Choose a Client ---")] + [(i.id, i.name) for i in current_user.clients.all()]

        request_args = copy_dict(request.args, {})

        view_filters = [{"name": "all", "value": "all"}, {"name": "admin", "value": "admin"},
                        {"name": "client", "value": "client"}]

        results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                      **{'customer_id': current_user.id,
                                                         'customer_read': True})

        if view_q and view_q != "all":
            if view_q == "admin":
                is_admin_msg = True
            else:
                is_admin_msg = False
            results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                          **{'customer_id': current_user.id,
                                                             'customer_read': True, "is_admin_message": is_admin_msg})

        if results.get("next", None):
            # build next page query parameters
            request_args["page"] = results.get("next")
            results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.get("prev", None):
            # build previous page query parameters
            request_args["page"] = results.get("prev")
            results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template('customer/messages/inbox.html', **locals())


    @c_blueprint.route('/messages/create/', methods=["GET", "POST"])
    def create_message():
        if request.method == "POST":
            next_url = request.args.get("next") or url_for(".messages")
            client = get_current_client()

            form = forms.ComposeMessageForm()
            form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                                 ("client", "Client")]
            form.client_id.choices = [(0, "--- Choose a Client ---")] + [(i.id, i.name) for i in current_user.clients.all()]

            if form.validate_on_submit():
                data = form.data
                recipient_type = data.get("recipient_type", "admin")
                message_type = data.get("message_type", "message")
                if recipient_type == "client":
                    data["is_admin_message"] = False
                    data["admin_read"] = False
                    data["client_read"] = False
                    data["client_inbox"] = True
                    data["client_outbox"] = False
                else:
                    data["is_admin_message"] = True
                    data["admin_read"] = False
                    data["client_read"] = False
                    data["client_inbox"] = False
                    data["client_outbox"] = False
                    data["client_id"] = None
                data["customer_id"] = current_user.id
                data["name"] = current_user.name
                data["email"] = current_user.email
                data["phone"] = current_user.phone
                data["customer_read"] = True
                data["customer_replied"] = False
                data["admin_replied"] = False
                data["client_replied"] = False
                process_form = forms.MessageProcessingForm(**data)
                process_form.validate()
                p_data = process_form.data
                if recipient_type == "admin":
                    p_data["is_admin_message"] = True
                timestamp = (datetime.today() - datetime(1970, 1, 1)).total_seconds()
                identifier = mongo.MongoService.get_next_sequence(message_type)
                resp = mongo.MongoService.record_data(message_type, identifier, timestamp, **p_data)
                return redirect(next_url)
            else:
                for key in form.errors:
                    try:
                        flash("%s - %s" % (key, form.errors[key][0]))
                    except:
                        flash("Error in information provided")
                    return redirect(next_url)
                return redirect(next_url)
        else:
            flash("Method Not Allowed")
            return redirect(url_for('.messages'))


    @c_blueprint.route('/messages/complaints/', methods=["GET"])
    def complaints():
        page_title = "Complaints"
        no_angular = True
        has_sub_lnk = True
        is_complaint = True

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
            view_q = request.args.get("view", None)
        except Exception, e:
            abort(404)

        client = get_current_client()
        search_place_holder = "Search"

        form = forms.ComposeMessageForm()
        form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                             ("client", "Client")]
        form.customer_id.choices = [(current_user.id, current_user.name)]
        form.client_id.choices = [(0, "--- Choose a Client ---")] + [(i.id, i.name) for i in current_user.clients.all()]

        request_args = copy_dict(request.args, {})

        results = mongo.MongoService.query_collection(collection_name='complaint', page=page, size=size,
                                                      **{'customer_id': current_user.id})

        if results.get("next", None):
            # build next page query parameters
            request_args["page"] = results.get("next")
            results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.get("prev", None):
            # build previous page query parameters
            request_args["page"] = results.get("prev")
            results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template('customer/messages/complaints.html', **locals())


    @c_blueprint.route('/reports/', methods=["GET"])
    @login_required
    def reports():
        g.user = current_user
        page_title = "Reports Overview"
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('customer/reports/index.html', **locals())


    @c_blueprint.route('/reports/download_report/<int:id>/')
    @login_required
    def download_report(id):
        page_title = "Download Report"
        obj = models.Report.query.get(int(id))

        if not obj:
            abort(404)

        # url = get_temp_cloudfiles_url(obj.handle, obj.name)
        url = obj.file_path
        return redirect(url)


    @c_blueprint.route('/reports/<string:handle>/report/')
    @login_required
    def report(handle):
        page_title = pageTitle = "%s Reports" % handle.title().replace("_", " ")
        no_angular = True
        has_sub_lnk = True
        if handle not in report_handles:
            abort(404)
        # Protect agains malicous paging parameters
        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
        except:
            abort(404)

        request_args = copy_dict(request.args, {})
        c_handle = "client_%s" % handle

        query = models.Report.query.filter(models.Report.handle == c_handle.lower(),models.Report.customer_id==current_user.id).order_by(
            desc(models.Report.date_created))

        is_report_list = True

        results = query.paginate(page, size, False)
        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template("customer/reports/lists.html", **locals())


    @c_blueprint.route('/reports/generate_report/<string:handle>/', methods=["GET", "POST"])
    @login_required
    def generate_report(handle):
        """
        Generate report matching handle and file type
        """

        page_title = pageTitle = "Generate %s Report" % handle.title()
        if handle not in report_handles:
            abort(404)
        no_angular = True
        has_sub_lnk = True
        is_report_gen = True
        user = current_user
        client = get_current_client()
        next_url = url_for(".report", handle=handle)

        type_choices = [("csv", "CSV"), ("xlsx", "XLSX")]
        form = forms.ReportForm()
        form.data_types.choices = type_choices
        form.data_type.choices = [(0, "---Select One---")] + type_choices

        if form.validate_on_submit():
            data = form.data
            data["handle"] = handle
            # data["client_id"] = client.id
            data["customer_id"] = current_user.id
            data.pop("data_types", "")
            # custom_report.delay(**data)
            customer_custom_report(**data)

            flash("The requested report is now being generated. An email will be sent to the admin shortly")

            return redirect(next_url)

        return render_domain_template("customer/reports/generate_report.html", **locals())


    @c_blueprint.route('/reports/individual_report/<string:handle>/', methods=["GET", "POST"])
    @login_required
    def individual_report(handle):
        page_title = pageTitle = "%s Report" % handle[:-1].capitalize()
        if handle not in report_handles:
            abort(404)
        search_q = request.args.get("q", None)
        no_angular = True
        has_sub_lnk = True
        is_individual_report = True
        return render_domain_template("customer/reports/individual_report.html", **locals())


    @c_blueprint.route('/reports/individual/<string:handle>/report/', methods=["GET", "POST"])
    @login_required
    def individual_report_chart(handle):
        if request.method == "POST":
            _data = {}
            if request.data:
                _data = request.data
                _data = json.loads(_data)
            elif request.form:
                _data = json.dumps(request.form)
                _data = json.loads(_data)

            q = _data.get("q")

            client = get_current_client()
            client_id = client.id

            if handle == "devices":
                device = models.Device.query.filter(models.Device.client_id == client_id,models.Device.customer_id==current_user.id,
                                                    models.Device.code == q).first()
                if not device:
                    return jsonify({"status": "failure", "payload": _data})
                _data["obj_id"] = device.id
                raw_metrics = device.product.metrics
                metrics = [i.as_dict for i in raw_metrics]
                _data["metrics"] = metrics
                _data["devices_json"] = [device.as_dict]
                _data["longitude"] = device.point.longitude if device.point else "0"
                _data["latitude"] = device.point.latitude if device.point else "0"
                _data["name"] = device.code
                return jsonify({"status": "success", "payload": _data})
            elif handle == "installations":
                installation = models.Installation.query.filter(models.Installation.client_id == client_id,
                                                                models.Installation.code == q).first()
                if not installation:
                    return jsonify({"status": "failure", "payload": _data})
                _data["obj_id"] = installation.id
                return jsonify({"status": "success", "payload": _data})
            elif handle == "stations":
                station = models.Station.query.filter(models.Station.client_id == client_id,
                                                      models.Station.code == q).first()
                if not station:
                    return jsonify({"status": "failure", "payload": _data})
                _data["obj_id"] = station.id
                _data["longitude"] = station.location.longitude if station.location else "0"
                _data["latitude"] = station.location.latitude if station.location else "0"
                _data["devices_json"] = [device.as_dict for device in station.devices]
                _data["name"] = station.name
                return jsonify({"status": "success", "payload": _data})
            else:
                return jsonify({"status": "failure", "payload": _data})

        return jsonify({"status": "failure", "payload": {"msg": "Request Method Not Allowed"}})


    @c_blueprint.route('/reports/individual/<string:handle>/<int:id>/report/', methods=["GET", "POST"])
    @login_required
    def generate_individual_report_chart(handle, id):
        no_report = render_domain_template("client/report/individuals/no-report.html", **locals())

        if handle == "devices":
            device = models.Device.query.filter(models.Device.id == id,models.Device.customer_id==current_user.id).first()
            return render_domain_template("client/report/individuals/device.html", **locals())
        elif handle == "installations":
            installation = models.Installation.query.filter(models.Installation.id == id).first()
            return render_domain_template("client/report/individuals/installation.html", **locals())
        elif handle == "stations":
            station = models.Station.query.filter(models.Station.id == id).first()
            return render_domain_template("customer/reports/individuals/station.html", **locals())
        else:
            return no_report


    @c_blueprint.route('/reports/no/report/', methods=["GET", "POST"])
    @login_required
    def no_report():
        return render_domain_template("customer/reports/individuals/no-report.html", **locals())

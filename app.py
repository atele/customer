import os

from model_package import models

import settings
from plant import setup_app

app = setup_app('customer_app', 'ProductionConfig', models.db)
db = app.db

with app.app_context():

    from views import c_blueprint

    app.register_blueprint(c_blueprint)

    if __name__ == '__main__':
        port = int(os.environ.get('PORT', 5620))
        app.run(host='0.0.0.0', port=port, debug=True)

/**
 * Created by stikks-workstation on 2/10/17.
 */

var app = angular.module('atele.models', ['ngResource']);


app.config(function() {
    // console.log($http);
    // $httpProvider.defaults.common['Authorization'] = 'Token ' + $rootScope.token;
});

// User Account resources
app.factory('User', function ($resource) {
    return $resource('/v1/users/:id/', { id: '@_id' }, {
        update: {
          method: 'PUT' // this method issues a PUT request
        },
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('Client', function ($resource) {
    return $resource('/v1/clients/:id/', { id: '@_id'}, {
        query:  {
            method:'GET', isArray:false
        },
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Customer', function ($resource) {
    return $resource('/v1/customers/:id', { id: '@_id'}, {
        query:  {
            method:'GET', isArray:false
        },
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('CustomerRequest', function ($resource) {
    return $resource('/v1/customer-requests/:id', { id: '@_id' }, {
        query: {
            method: 'GET', isArray: false
        }
    });
});


// Default system resources
app.factory('City', function ($resource) {
    return $resource('/v1/cities/:id/', { id: '@_id' }, {
        query:  {
            method:'GET', isArray:false
        },
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('State', function ($resource) {
    return $resource('/v1/states/:id/', { id: '@_id'}, {
        query:  {
            method:'GET', isArray:false
        },
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Country', function ($resource) {
    return $resource('/v1/countries/:id', { id: '@_id'}, {
        query:  {
            method:'GET', isArray:false
        },
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Currency', function ($resource) {
    return $resource('/v1/currencies/:id/', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Address', function ($resource) {
    return $resource('/v1/addresses/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('TimeZone', function ($resource) {
    return $resource('/v1/timezones/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('AccessGroup', function ($resource) {
    return $resource('/v1/access-groups/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Role', function ($resource) {
    return $resource('/v1/roles/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Settings', function ($resource) {
    return $resource('/v1/settings/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('CustomerSettings', function ($resource) {
    return $resource('/v1/customer-settings/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Service', function ($resource) {
    return $resource('/v1/services/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('AlertType', function ($resource) {
    return $resource('/v1/alert_types/:id', { id: '@_id'}, {
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('InstallationType', function ($resource) {
    return $resource('/v1/installation_types/:id', { id: '@_id'}, {
        query:  {
            method:'GET', isArray:false
        }
    });
});

// Device resources
app.factory('DeviceType', function ($resource) {
    return $resource('/v1/device-types/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Product', function ($resource) {
    return $resource('/v1/products/:id', { id: '@_id'}, {
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('Device', function ($resource) {
    return $resource('/v1/devices/:id', { id: '@_id' }, {
        query: {
            method: 'GET', isArray: false
        },
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('DeviceRequest', function ($resource) {
    return $resource('/v1/device-requests/:id', { id: '@_id' }, {
        query: {
            method: 'GET', isArray: false
        }
    });
});

app.factory('Mqtt', function ($resource) {
    return $resource('/v1/mqtt/:ser', { id: '@_id' }, {
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('Network', function ($resource) {
    return $resource('/v1/networks/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        },
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('NetworkRequest', function ($resource) {
    return $resource('/v1/network-requests/:id', { id: '@_id' }, {
        query: {
            method: 'GET', isArray: false
        }
    });
});

app.factory('Installation', function ($resource) {
    return $resource('/v1/installations/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        },
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('Station', function ($resource) {
    return $resource('/v1/sub_stations/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        },
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('Alert', function ($resource) {
    return $resource('/v1/alerts/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        },
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('AlertRequest', function ($resource) {
    return $resource('/v1/alert-requests/:id', { id: '@_id' }, {
        query: {
            method: 'GET', isArray: false
        }
    });
});


// Payment information
app.factory('Wallet', function ($resource) {
    return $resource('/v1/wallets/:id/', { id: '@_id' }, {
        update: {
          method: 'PUT' // this method issues a PUT request
        },
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('Billing', function ($resource) {
    return $resource('/v1/billings/:id/', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('ClientBilling', function ($resource) {
    return $resource('/v1/client-billings/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('BillingRecord', function ($resource) {
    return $resource('/v1/billing-records/:id/', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('ClientBillingRecord', function ($resource) {
    return $resource('/v1/client-billing-records/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Card', function ($resource) {
    return $resource('/v1/cards/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Invoice', function ($resource) {
    return $resource('/v1/invoices/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Subscription', function ($resource) {
    return $resource('/v1/subscriptions/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

// mongo DB
app.factory('Fault', function ($resource) {
    return $resource('/v1/mongo/faults/:id/', { id: '@_id' }, {
        query:  {
            method:'GET', isArray:false
        }
    });
});

app.factory('Notification', function ($resource) {
   return $resource('/v1/mongo/notifications/:id/', {id: '@_id'}, {
       query: {
           method: 'GET', isArray: false
       }
   })
});
/**
 * Created by stikks-workstation on 2/10/17.
 */
'use strict';
var app = angular.module('atele', ['ngRoute', 'angular-loading-bar', 'ngAnimate', 'ngSanitize', 'angular-jwt',
    'ngMaterial', 'atele.models', 'atele.services', 'atele.routes', 'atele.controllers.basic', 'atele.controllers.alert',
    'atele.controllers.equipment', 'atele.controllers.customer']);

app.config(function ($httpProvider, $interpolateProvider, cfpLoadingBarProvider, jwtOptionsProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    cfpLoadingBarProvider.includeSpinner = true;

    jwtOptionsProvider.config({
        tokenGetter: ['TokenService', function (TokenService) {
            return TokenService.retrieve();
        }]
    });

    $httpProvider.interceptors.push('jwtInterceptor');

    //$httpProvider.interceptors.push(function ($q, cfpLoadingBar) {
    //    return {
    //        'request': function (config) {
    //            cfpLoadingBar.start();
    //            cfpLoadingBar.inc();
    //            return config;
    //        },
    //        'response': function (response) {
    //            console.log(response);
    //            cfpLoadingBar.complete();
    //            return response;
    //        }
    //    }
    //});
});

app.run(['$rootScope', function ($root) {

    $root.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        var nav = $root.navigations.find(function (elem) {
            var _name = toState.url.split('/');
            return elem.code == _name[1]
        });

        if (nav) {
            $root.current_nav.sub_navs = nav.sub_navs;
        } else {
            $root.current_nav.sub_navs = []
        }
    });
    //$root.$on('$routeChangeStart', function (e, curr, prev) {
    //    if (curr.$$route && curr.$$route.resolve) {
    //        // Show a loading message until promises aren't resolved
    //        console.log('resolving')
    //    }
    //    else {
    //        console.log('no resolve')
    //    }
    //});
    //
    //$root.$on('$routeChangeSuccess', function (event, current, previous) {
    //    var nav = $root.navigations.find(function (elem) {
    //        var _name = current.$$route.originalPath.split('/');
    //        return elem.code == _name[1]
    //    });
    //
    //    if (nav) {
    //        $root.current_nav.sub_navs = nav.sub_navs;
    //    } else {
    //        $root.current_nav.sub_navs = []
    //    }
    //});

}]);
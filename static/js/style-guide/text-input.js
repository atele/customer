$(function() {
  var textInput = '[type="text"], [type="password"], [type="number"], [type="email"], [type="tel"]';

  var updateInput = function($el) {
    if ($el.val()) {
      $el.parent().addClass('has-value');
    } else {
      $el.parent().removeClass('has-value');
    }
    if ($el.is(':focus')) {
      $el.parent().addClass("is-focused");
    } else {
      $el.parent().removeClass("is-focused");
    }
  };

  $(textInput).on('blur focus change', function() {
    updateInput($(this));
  }).each(function() {
    updateInput($(this));
  });
});

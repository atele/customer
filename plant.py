from logging import Formatter, ERROR, handlers

from flask import Flask
from flask_script import Manager
from flask_bcrypt import Bcrypt
from flask_assets import Environment
from flask_cache import Cache
from flask_login import LoginManager
from flask_marshmallow import Marshmallow
from flask_sitemap import Sitemap
from flask_principal import Principal
from celery import Celery
from raven.contrib.flask import Sentry
import htmlmin

import settings
from integrations_package.reporting import Reporting

def render_domain_template(template_name, **kwargs):
    from flask import render_template

    _html = render_template(template_name, **kwargs)

    return htmlmin.minify(_html, remove_empty_space=True, remove_comments=True)


def make_celery(app):
    """ Enables celery to run within the flask application context """

    celery = Celery(app.import_name, backend=app.config.get("CELERY_RESULT_BACKEND"),
                    broker=app.config.get("CELERY_BROKER_URL"))
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return celery.Task.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def setup_app(name, config_class, db):
    module = getattr(settings, config_class)
    app = Flask(name, template_folder='templates', static_folder='static')
    app.config.from_object(module)

    db.init_app(app)
    app.db = db

    # flask script manager
    app.manager = Manager(app)

    # inject celery into the app
    app.celery = make_celery(app)

    # password encryption
    app.bcrypt = Bcrypt(app)

    # initializing assets - js/css/sass
    app.assets = Environment(app)
    app.assets.from_yaml('asset.yml')

    # Initialize Flutterwave
    # app.flutterwave = Flutterwave(app)

    # templates caching
    app.cache = Cache(app, config={'CACHE_TYPE': 'simple'})

    # sitemap
    app.ext = Sitemap(app)

    # principal identity management
    app.principal = Principal(app)

    app.reporting = Reporting(app)

    # Initializing login manager
    login_manager = LoginManager()
    # login_manager.login_view = "c_blueprint.login"
    login_manager.blueprint_login_views = {'c_blueprint': '/login'}
    login_manager.session_protection = 'strong'
    login_manager.init_app(app)
    app.login_manager = login_manager

    # flask marshmallow api implementation of database models
    app.marshmallow = Marshmallow(app)

    file_handler = handlers.RotatingFileHandler("/var/log/customer/%s.log" % app.config.get("LOGFILE_NAME", name),
                                                maxBytes=500 * 1024)
    file_handler.setLevel(ERROR)
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    ))
    app.logger.addHandler(file_handler)

    # configure sentry
    if not app.config.get("DEBUG", True):
        sentry = Sentry(app)
        app.sentry = sentry

    return app
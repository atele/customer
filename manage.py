#!/usr/bin/env python
import os

from flask_migrate import MigrateCommand
from flask_assets import ManageAssets

from model_package import models

from plant import setup_app

app = setup_app('customer_app', 'DevelopmentConfig', models.db)
manager = app.manager
manager.add_command('assets', ManageAssets)

# Allow the manager access config files on the fly
manager.add_option('-c', '--config', dest='config_obj', default='settings.DevelopmentConfig', required=False)


with app.app_context():
    @manager.command
    def run():
        from views import c_blueprint

        app.register_blueprint(c_blueprint)

        port = int(os.environ.get('PORT', 5620))
        app.run(host='0.0.0.0', port=port)


    if __name__ == "__main__":
        manager.run()
